module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
};

const moduleResolverPlugin = [
  'module-resolver',
  {
    root: ['.'],
    extensions: [
      '.ios.js',
      '.android.js',
      '.js',
      '.d.ts',
      '.ts',
      '.tsx',
      '.json',
    ],
    alias: {
      '@': './src',
    },
  },
];

const reactNativeReanimatedPlugin = [
  'react-native-reanimated/plugin',
  {
    globals: ['__scanCodes'],
  },
];

module.exports = function (api) {
  api.cache(true);
  return {
    presets: ['module:metro-react-native-babel-preset'],
    plugins: [
      [
        'module:react-native-dotenv',
        {
          moduleName: '@env',
          path: '.env',
          blacklist: null,
          whitelist: null,
          safe: false,
          allowUndefined: true,
        },
      ],
      moduleResolverPlugin,
      reactNativeReanimatedPlugin,
    ],
  };
};
